from pygame_roar import utils


def test_generate_time_string_00():
    milliseconds = 5876892
    actual = utils.generate_time_string(milliseconds)
    assert actual == '01:37:56.892'


def test_generate_time_string_01():
    milliseconds = 5876892
    actual = utils.generate_time_string(milliseconds, '%M:%S.%s')
    assert actual == '97:56.892'


def test_generate_time_string_02():
    milliseconds = 5876892
    actual = utils.generate_time_string(milliseconds, '%S.%s')
    assert actual == '5876.892'
