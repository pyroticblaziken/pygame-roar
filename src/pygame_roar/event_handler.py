from abc import ABC, abstractmethod
from pygame.event import Event

class EventHandler(ABC):
    """Abstract class to define how handle event methods will hold true
    """
    @abstractmethod
    def handle_event(self: 'EventHandler', events: list[Event]) -> None:
        """Method to be called to handle events
        """
        raise NotImplementedError
