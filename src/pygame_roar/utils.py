from enum import Enum, auto
from pygame import Color, Surface, BLEND_MULT
from pygame.font import Font
from pygame.mask import from_surface
import logging
import os


def get_logger() -> logging.Logger:
    logger = None

    if not logger: 
        logging.basicConfig(level=os.environ.get("PG_ROAR_LOG_LEVEL", "ERROR"))
        logger = logging.getLogger(__name__) # CRITICAL, ERROR, WARNING, INFO, DEBUG, NOTSET

    return logger


def clamp(value, minimum, maximum):
    return min(max(value, minimum), maximum)


def within_bounds(value, minimum, maximum) -> bool:
    return value >= minimum and value <= maximum


class TypographicalAlignment(Enum):
    LEFT = auto()
    CENTER = auto()
    RIGHT = auto()


def render_wrapped_text(font: Font, text: str, antialias: bool, color: Color, max_width: int, background: Color = None, alignment: TypographicalAlignment = TypographicalAlignment.LEFT) -> Surface:
    words = text.split()
    lines = []

    while len(words) > 0:
        _line = []
        while len(words) > 0:
            _line.append(words.pop(0))
            fw, _ = font.size(' '.join(_line + words[:1]))
            if fw > max_width:
                break
        
        line = ' '.join(_line)
        lines.append(line)
    
    sizes = [font.size(line) for line in lines]
    height = sum([s[1] for s in sizes])
    width = max([s[0] for s in sizes])
    rendered_text = Surface((width, height))

    if background: rendered_text.fill(background)

    y_offset = 0
    for line in lines:
        _tw, _th = font.size(line)
        _text = font.render(line, antialias, color, background)
        match alignment:
            case TypographicalAlignment.LEFT:
                pos = (0, y_offset)
            case TypographicalAlignment.CENTER:
                pos = ((width - _tw) / 2, y_offset)
            case TypographicalAlignment.RIGHT:
                pos = (width - _tw, y_offset)
            
        rendered_text.blit(_text, pos)
        y_offset += _th

    return rendered_text


def generate_time_string(milliseconds: int, format: str = '%H:%M:%S.%s') -> str:
    return f'{(milliseconds // 3600000):02}:{((milliseconds // 60000) % 60):02}:{((milliseconds // 1000) % 60):02}.{(milliseconds % 1000):03}'


def color_image(image: Surface, color) -> Surface:
    colored_img = Surface(image.get_size())
    colored_img.fill(color)

    final_img = image.copy()
    final_img.blit(colored_img, (0, 0), special_flags=BLEND_MULT)
    return final_img


# How do I import _ColorValue ?!?!
def generate_outline(surface: Surface, outline, background) -> Surface:
    mask = from_surface(surface)
    mask_img = mask.to_surface()
    mask_img.set_colorkey([0, 0, 0])
    final_image = Surface((surface.get_width() + 2, surface.get_height() + 2 ))
    final_image.fill(background)
    final_image.blit(mask_img, (0, 0))
    final_image.blit(mask_img, (0, 2))
    final_image.blit(mask_img, (2, 0))
    final_image.blit(mask_img, (2, 2))
    final_image.blit(surface, (1, 1))
    return final_image
