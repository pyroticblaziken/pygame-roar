from pygame import KEYDOWN, KEYUP, MOUSEWHEEL, QUIT
from pygame.event import Event
from pygame import joystick, mouse
from .controller import Controller, ButtonType, AxisType, ControllerLayout
from .config_manager import ConfigManager


class InputManager():
    """Handle inputs from the player(s)"""
    def __init__(self: 'InputManager', config_manager: ConfigManager) -> None:
        joystick.init()
        self.events: list[Event] = []
        self.config_manager: ConfigManager = config_manager
        self.player_controllers: list[Controller] = []
        self.joysticks: list[joystick.Joystick] = [joystick.Joystick(i) for i in range(joystick.get_count())]
        for i, map in enumerate(self.config_manager.player_key_mappings):
            self.player_controllers.append(Controller(i, map))
    
    def handle_events(self: 'InputManager', events: list[Event]) -> bool:
        self.events = events
        for event in events:
            for controller in self.player_controllers:
                if event.type == KEYDOWN:
                    btn = next((k for k, v in controller.mapping.items() if event.key in v), None)
                    if btn: controller.buttons.add(btn)
                elif event.type == KEYUP:
                    btn = next((k for k, v in controller.mapping.items() if event.key in v), None)
                    if btn: controller.buttons.remove(btn)
                elif event.type == MOUSEWHEEL:
                    controller.axes[AxisType.MOUSE_WHEEL] = (event.x, event.y)

            if controller.layout == ControllerLayout.KEYBOARD:
                controller.axes[AxisType.MOUSE_POSITION] = mouse.get_pos()

        return any(e.type == QUIT for e in events)


class StaticInputManager():
    instance: InputManager = None

    @classmethod
    def get_player_controller(cls: 'StaticInputManager', index: int) -> Controller:
        return cls.instance.player_controllers[index]

    @classmethod
    def get_player_controllers(cls: 'StaticInputManager') -> list[Controller]:
        return cls.instance.player_controllers
    
    @classmethod
    def get_raw_events(cls: 'StaticInputManager') -> list[Event]:
        return cls.instance.events
