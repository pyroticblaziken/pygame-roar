import abc

class Updatable(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'update') and
                callable(subclass.update))
    
    
    @abc.abstractmethod
    def update(self: 'Updatable', time: int):
        """Perform any game actions based on time elapsed"""
        raise NotImplementedError