class GameTime():
    """Pygame Roar's time storage class
    """

    @property
    def days(self: 'GameTime') -> int:
        return self._days
    
    @days.setter
    def days(self: 'GameTime', value: int) -> None:
        self._days = value
    
    @property
    def hours(self: 'GameTime') -> int:
        return self._hours
    
    @hours.setter
    def hours(self: 'GameTime', value: int) -> None:
        self._hours = value

    @property
    def minutes(self: 'GameTime') -> int:
        return self._minutes
    
    @minutes.setter
    def minutes(self: 'GameTime', value: int) -> None:
        self._minutes = value

    @property
    def seconds(self: 'GameTime') -> int:
        return self._seconds
    
    @seconds.setter
    def seconds(self: 'GameTime', value: int) -> None:
        self._seconds = value

    @property
    def milliseconds(self: 'GameTime') -> int:
        return self._milliseconds
    
    @milliseconds.setter
    def milliseconds(self: 'GameTime', value: int) -> None:
        self._milliseconds = value

    def __init__(self, days: int=0, hours: int=0, minutes: int=0, seconds: int=0, milliseconds: int=0):
        """
        """
        self._days:int = days
        self._hours:int = hours
        self._minutes: int = minutes
        self._seconds: int = seconds
        self._milliseconds: int = milliseconds
    
    def __repr__(self: 'GameTime') -> str:
        return f'pygame_roar.GameTime({self.days}, {self.hours}, {self.minutes}, {self.seconds}, {self.milliseconds})'

    def __str__(self: 'GameTime') -> str:
        return f'{self.days}:{self.hours:02}:{self.minutes:02}:{self.seconds:02}.{self.milliseconds:02}'
    
    def __format__(self: 'GameTime', format_spec: str):
        _days = self.days
        _hours = self.hours
        _minutes = self.minutes
        _seconds = self.seconds
        _milliseconds = self.milliseconds

        if not '%d' in format_spec: _hours += 24 * _days
        if not '%H' in format_spec: _minutes += 60 * _hours
        if not '%M' in format_spec: _seconds += 60 * _minutes
        if not '%S' in format_spec: _milliseconds += 1000 * _seconds

        return format_spec.replace('%d', f'{self.days}').replace('%H', f'{self.hours:02}').replace('%M', f'{self.minutes:02}').replace('%S', f'{self.seconds:02}').replace('%s', f'{self.milliseconds:03}')

    def __lt__(self: 'GameTime', other: 'GameTime') -> bool:
        if (self.days != other.days): return self.days < other.days
        if (self.hours != other.hours): return self.hours < other.hours
        if (self.minutes != other.minutes): return self.minutes < other.minutes
        if (self.seconds != other.seconds): return self.seconds < other.seconds
        if (self.milliseconds != other.milliseconds): return self.milliseconds < other.milliseconds
        return False # They are equal in this case 

    def __le__(self: 'GameTime', other: 'GameTime') -> bool:
        if (self == other): return True
        if (self.days != other.days): return self.days < other.days
        if (self.hours != other.hours): return self.hours < other.hours
        if (self.minutes != other.minutes): return self.minutes < other.minutes
        if (self.seconds != other.seconds): return self.seconds < other.seconds
        if (self.milliseconds != other.milliseconds): return self.milliseconds < other.milliseconds
        return False # This should not be reachable

    def __eq__(self: 'GameTime', other: 'GameTime') -> bool:
        return self.days == other.days and self.hours == other.hours and self.minutes == other.minutes and self.seconds == other.seconds and self.milliseconds == other.milliseconds
    
    def __ne__(self: 'GameTime', other: 'GameTime') -> bool:
        return not (self == other)
    
    def __gt__(self: 'GameTime', other: 'GameTime') -> bool:
        if (self.days != other.days): return self.days > other.days
        if (self.hours != other.hours): return self.hours > other.hours
        if (self.minutes != other.minutes): return self.minutes > other.minutes
        if (self.seconds != other.seconds): return self.seconds > other.seconds
        if (self.milliseconds != other.milliseconds): return self.milliseconds > other.milliseconds
        return False # They are equal in this case
    
    def __ge__(self: 'GameTime', other: 'GameTime') -> bool:
        if (self == other): return True
        if (self.days != other.days): return self.days > other.days
        if (self.hours != other.hours): return self.hours > other.hours
        if (self.minutes != other.minutes): return self.minutes > other.minutes
        if (self.seconds != other.seconds): return self.seconds > other.seconds
        if (self.milliseconds != other.milliseconds): return self.milliseconds > other.milliseconds
        return False # They are equal in this case 

    def __hash__(self: 'GameTime') -> int:
        return hash((self.days, self.hours, self.minutes, self.seconds, self.milliseconds))

    def tick(self: 'GameTime', milliseconds: int) -> None:
        self.milliseconds += milliseconds
    
        if (self.milliseconds >= 1000):
            self.seconds += self.milliseconds // 1000
            self.milliseconds %= 1000

        if (self.seconds >= 60):
            self.minutes += self.seconds // 60
            self.seconds %= 60
        
        if (self.minutes >= 60):
            self.hours += self.minutes // 60
            self.minutes %= 60
        
        if (self.hours >= 24):
            self.days += self.hours // 24
            self.hours %= 24
