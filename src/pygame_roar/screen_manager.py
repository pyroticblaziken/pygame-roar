from abc import ABC, abstractmethod
from enum import Enum, auto
from pygame.event import Event
from pygame import Color, Surface
from .game_screen import GameScreen
from .config_manager import StaticConfigManager


class TransitionType(Enum):
    FADE_IN = auto()
    FADE_OUT = auto()
    SLIDE_LEFT = auto()
    SLIDE_RIGHT = auto()


class ScreenManager(ABC):
    """Manage the GameScreens and Transitions"""
    CLEAR_COLOR: Color = Color('magenta') # https://www.pygame.org/docs/ref/color_list.html

    def __init__(self: 'ScreenManager'):
        self.screens: list[GameScreen] = []
        self.surface: Surface = Surface(StaticConfigManager.game_size())
        self.transitioning: bool = False
        self.transition_type: TransitionType = TransitionType.SLIDE_LEFT
        self.transition_time: int = 1000 # milliseconds
        self.active_transition_time: int = 0
    
    @property
    def transition_time(self) -> int:
        return self._transition_time
    
    @transition_time.setter
    def transition_time(self, value: int):
        self._transition_time = value

    def add_screen(self: 'ScreenManager', screen: GameScreen) -> None:
        screen.load_content()
        self.screens.append(screen)
    
    def update(self: 'ScreenManager', time: int) -> None:
        if self.transitioning:
            self.active_transition_time += time
            if self.active_transition_time >= self.transition_time:
                self.screens = list(filter(lambda s: not s.is_exiting, self.screens))
                self.transitioning = False
        [s.update(time, self.transitioning) for s in self.screens]

    def draw_slide_left(self: 'ScreenManager', index: int) -> None:
        t_state = self.active_transition_time / self.transition_time
        adjust = int(StaticConfigManager.game_size()[0] * t_state)
        leaving_pos = (-adjust, 0)
        entering_pos = (StaticConfigManager.game_size()[0] - adjust, 0)
        for i, screen in enumerate(self.screens):
            if i < index:    
                self.surface.blit(screen.screen, (0, 0))
            elif i == index:
                self.surface.blit(screen.screen, leaving_pos)
            else:
                self.surface.blit(screen.screen, entering_pos)
    
    def draw_slide_right(self: 'ScreenManager', index: int) -> None:
        t_state = self.active_transition_time / self.transition_time
        adjust = int(StaticConfigManager.game_size()[0] * t_state)
        leaving_pos = (adjust, 0)
        entering_pos = (- StaticConfigManager.game_size()[0] + adjust, 0)
        for i, screen in enumerate(self.screens):
            if i < index:    
                self.surface.blit(screen.screen, (0, 0))
            elif i == index:
                self.surface.blit(screen.screen, leaving_pos)
            else:
                self.surface.blit(screen.screen, entering_pos)

    def draw(self: 'ScreenManager', window: Surface) -> None:
        self.surface.fill(GameScreen.CLEAR_COLOR)
        self.surface.set_colorkey(GameScreen.CLEAR_COLOR)
        [s.draw() for s in self.screens]
        if not self.transitioning:
            [self.surface.blit(s.screen, (0, 0)) for s in self.screens]
        else:
            index = [i for i, s in enumerate(self.screens) if s.is_exiting][0]
            match self.transition_type:
                case TransitionType.SLIDE_LEFT:
                    self.draw_slide_left(index)
                case TransitionType.SLIDE_RIGHT:
                    self.draw_slide_right(index)
                case _:
                    [self.surface.blit(s.screen, (0, 0)) for s in self.screens]

        window.blit(self.surface, (0, 0))

    @abstractmethod
    def handle_events(self: 'ScreenManager', events: Event) -> None:
        pass

    # def start_transition(self: 'ScreenManager', type: TransitionType, time_span: int) -> None:
    #     self.trasition_time = time_span
    #     self.active_transition_time = 0
    #     self.transition_type = type
    #     self.transitioning = True
