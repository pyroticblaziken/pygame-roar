from . import display_help
import logging
import os

def main():
    logging.basicConfig(level=os.environ.get("PG_ROAR_LOG_LEVEL", "ERROR"))


if __name__ == '__main__':
    display_help()
