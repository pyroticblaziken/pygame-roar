"""This module holds the code for the Game object which is responsible for maintianing the game state.
"""
from abc import ABC, abstractmethod
import pygame

from .audio_manager import AudioManager
from .config_manager import ConfigManager, StaticConfigManager
from .input_manager import InputManager, StaticInputManager
from .resource_manager import ResourceManager, StaticResourceManager
from .screen_manager import ScreenManager
from .utils import get_logger

logger = get_logger()

class Game(ABC):
    """Game object:
    """
    # @property
    # def audio_manager(self: 'Game') -> AudioManager:
    #     return self._audio_manager

    @property
    def config_manager(self: 'Game') -> ConfigManager:
        return self._config_manager
    
    @config_manager.setter
    def config_manager(self: 'Game', value: ConfigManager) -> None:
        self._config_manager = value
        StaticConfigManager.instance = self._config_manager
    
    @property
    def input_manager(self: 'Game') -> InputManager:
        return self._input_manager
    
    @input_manager.setter
    def input_manager(self: 'Game', value: InputManager):
        self._input_manager = value
        StaticInputManager.instance = self._input_manager
    
    @property
    def resource_manager(self: 'Game') -> ResourceManager:
        return self._resource_manager

    @resource_manager.setter
    def resource_manager(self: 'Game', value: ResourceManager) -> None:
        self._resource_manager = value
        StaticResourceManager.instance = self._resource_manager
    
    # @property
    # def screen_manager(self: 'Game') -> ScreenManager:
    #     return self._screen_manager

    def __init__(self: 'Game'):
        """Create a game object"""
        self.audio_manager: AudioManager = None
        self.config_manager: ConfigManager = None
        self.input_manager: InputManager = None
        self.resource_manager: ResourceManager = None
        self.screen_manager: ScreenManager = None
        self.window: pygame.Surface = None
        self.clock: pygame.time.Clock = None
        self.joysticks: list = None
    
    @abstractmethod
    def init_audio_manager(self: 'Game') -> None:
        raise NotImplementedError

    def init_config_manager(self: 'Game') -> None:
        self.config_manager = ConfigManager()

    def init_input_manager(self: 'Game') -> None:
        self.input_manager = InputManager(self.config_manager)
        
    def init_resource_manager(self: 'Game') -> None:
        self.resource_manager = ResourceManager()
    
    @abstractmethod
    def init_screen_manager(self: 'Game') -> None:
        raise NotImplementedError

    def init(self: 'Game'):
        pygame.init()
        self.init_config_manager()
        self.config_manager.load_config()
        self.init_resource_manager()
        self.window = pygame.display.set_mode(self.config_manager.window_size, self.config_manager.display_mode_flags(), vsync=1 if self.config_manager.vsync else 0)
        self.resource_manager.load_assets()
        self.init_input_manager()
        self.init_audio_manager()
        self.init_screen_manager()
        pygame.display.set_caption(self.config_manager.app_name)
        self.clock = pygame.time.Clock()

    def handle_events(self: 'Game', events: list[pygame.event.Event]):
        for event in events:
            if event.type == pygame.VIDEORESIZE:
                self.window = pygame.display.set_mode(self.config_manager.window_size, self.config_manager.display_mode_flags(), vsync=1 if self.config_manager.vsync else 0)
    
    def run(self: 'Game') -> bool:
        time = self.clock.tick(self.config_manager.fps)
        events = pygame.event.get()
        shouldQuit = self.input_manager.handle_events(events)
        self.handle_events(events)
        self.config_manager.handle_events(events)
        self.screen_manager.handle_events(events)
        self.audio_manager.handle_events(events)
        self.window.fill((200, 200, 200))
        self.screen_manager.update(time)
        self.screen_manager.draw(self.window)
        pygame.display.update()
        return shouldQuit
    
    def quit(self: 'Game') -> None:
        try :
            self.config_manager.save_config()
        except BaseException as err:
            logger.error(f'Unable to save config: {err=}, {type(err)=}')


        pygame.quit()
