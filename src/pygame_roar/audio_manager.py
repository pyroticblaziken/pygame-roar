from abc import ABC, abstractmethod
from pygame import mixer
from pygame.event import Event


class AudioManager(ABC):
    """Manage the audio for the game

    This should use events and custom events to trigger playing sounds
    """
    def __init__(self: 'AudioManager') -> None:
        mixer.set_num_channels(64)

    @abstractmethod
    def handle_events(self: 'AudioManager', events: list[Event]) -> None:
        raise NotImplementedError
