from enum import Enum, auto
from abc import ABC, abstractmethod
from pygame import Color, Surface
from .config_manager import StaticConfigManager


class ScreenState(Enum):
    TRANSITION_OFF = auto()
    TRANSITION_ON = auto()
    ACTIVE = auto()
    INACTIVE = auto()
    HIDDEN = auto()


class GameScreen(ABC):
    CLEAR_COLOR: Color = Color('magenta') # https://www.pygame.org/docs/ref/color_list.html

    def __init__(self: 'GameScreen'):
        self.other_screen_has_focus: bool = False
        self.covered: bool = False
        self.screen_state: ScreenState = ScreenState.TRANSITION_ON
        self.is_exiting = False
        self.is_popup = False
        self.transition_position = 0
        self.screen = Surface(StaticConfigManager.game_size())
    
    # Properties? - https://www.programiz.com/python-programming/property

    @abstractmethod
    def load_content(self: 'GameScreen') -> None:
        """Load all the content necessary to run the screen properly"""
        raise NotImplementedError

    @abstractmethod
    def update(self: 'GameScreen', time: int, is_transitioning: bool):
        raise NotImplementedError

    def draw(self: 'GameScreen'):
        self.screen.fill(GameScreen.CLEAR_COLOR)
        self.screen.set_colorkey(GameScreen.CLEAR_COLOR)
