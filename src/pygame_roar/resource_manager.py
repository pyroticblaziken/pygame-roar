"""Resource Manager for this module.

This class allows the user to quickly access files loaded as part of the games run time.
"""

from os import path
import json
from pygame import Surface, font
from pygame.mixer import Sound
import pygame


class ResourceManager():
    """Load assets from specific folder"""
    def __init__(self: 'ResourceManager', base_asset_path: list[str] = ['assets'], resources_json: str = 'resources.json') -> None:
        self.base_asset_path: list[str] = base_asset_path
        self.resources_json: str = resources_json
        self.images: dict[str, Surface] = {}
        self.fonts: dict[str, font.Font] = {}
        self.sounds: dict[str, Sound] = {}
        self.data: dict[str, object] = {}

    def load_assets(self: 'ResourceManager') -> None:
        """Use resources.json to preload all of the assets for the game"""
        resources_json_path = path.join(*self.base_asset_path, self.resources_json)
        if path.exists(resources_json_path):
            with open(resources_json_path) as r_file:
                data = r_file.read()
            
            resources = json.loads(data)

            for img_data in resources['images']:
                self.images[img_data['id']] = pygame.image.load(path.join('assets', *img_data['path_parts'])).convert_alpha()
            
            for font_data in resources['fonts']:
                self.fonts[font_data['id']] = pygame.font.Font(path.join('assets', *font_data['path_parts']), font_data['size'])
            
            for sound_data in resources['sounds']:
                self.sounds[sound_data['id']] = Sound(path.join('assets', *sound_data['path_parts']))

            for object_data in resources['data']:
                with open(path.join('assets', *object_data['path_parts']), 'r') as file:
                    self.data[object_data['id']] = json.load(file)
    
    def get_image(self: 'ResourceManager', id: str) -> Surface:
        if not id in self.images: return None
        return self.images[id]
    
    def get_font(self: 'ResourceManager', id: str) -> font.Font:
        if not id in self.fonts: return None
        return self.fonts[id]
    
    def get_sound(self: 'ResourceManager', id: str) -> Sound:
        if not id in self.sounds: return None
        return self.sounds[id]
    
    def get_data(self: 'ResourceManager', id: str) -> object:
        if not id in self.data: return None
        return self.data[id]


class StaticResourceManager():
    instance: ResourceManager = None

    @classmethod
    def get_font(cls: 'StaticResourceManager', id: str) -> font.Font:
        return cls.instance.get_font(id)

    @classmethod
    def get_image(cls: 'StaticResourceManager', id: str) -> Surface:
        return cls.instance.get_image(id)
    
    @classmethod
    def get_sound(cls: 'StaticResourceManager', id: str) -> Sound:
        return cls.instance.get_sound(id)
    
    @classmethod
    def get_data(cls: 'StaticResourceManager', id: str) -> object:
        return cls.instance.get_data(id)
