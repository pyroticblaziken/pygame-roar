import abc
from pygame import Surface

class Drawable(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'draw') and
                callable(subclass.draw))
    

    @abc.abstractmethod
    def draw(self: 'Drawable', screen: Surface):
        """Render something to the screen"""
        raise NotImplementedError
