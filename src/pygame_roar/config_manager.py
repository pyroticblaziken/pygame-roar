from appdirs import user_data_dir
from abc import ABC
from copy import copy, deepcopy
from os import makedirs, path
import json
import logging
import pygame
from pygame.event import custom_type, post, Event
from pygame.constants import K_w, K_a, K_s, K_d, K_j, K_k, K_l, K_m, K_COMMA, K_SEMICOLON, K_RETURN, K_SLASH
from .controller import ButtonType, ControllerLayout
from .utils import clamp, get_logger
import logging

logger = logging.getLogger(__name__)

UPDATE_CONFIG = custom_type()
# Event Layout
#   type: int = UPDATE_CONFIG
#   key: str = attribute to be updated
#   value: any = value to set attribute to

DEFAULT_KEYBOARD_KEYMAP = {
    ButtonType.UP: [K_w],
    ButtonType.LEFT: [K_a],
    ButtonType.DOWN: [K_s],
    ButtonType.RIGHT: [K_d],
    ButtonType.BUTTON_1: [K_j],
    ButtonType.BUTTON_2: [K_k],
    ButtonType.BUTTON_3: [K_l],
    ButtonType.BUTTON_4: [K_SEMICOLON],
    ButtonType.BUTTON_5: [K_m],
    ButtonType.BUTTON_6: [K_COMMA],
    ButtonType.BUTTON_7: [K_SLASH],
    ButtonType.BUTTON_8: [K_RETURN],
}

DEFAULT_GAME_SIZE = (640, 480)
DEFAULT_SCREEN_SIZE = (640, 480)
DEFAULT_FPS = 30
DEFAULT_APP_NAME = 'MyApp'
DEFAULT_AUTHOR = 'SomeEntity'

TRIGGER_SCREEN_REFRESH = ['scaled', 'fullscreen', 'vsync']

class ConfigManagerEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ConfigManager):
            data = {}
            # Compose Data here
            data["fullscreen"] = obj.fullscreen
            data["scaled"] = obj.scaled
            data["vsync"] = obj.vsync
            data["master_volume"] = obj.master_volume
            return data
        return json.JSONEncoder.default(self, obj)

class ConfigManager(object):
    """Saves and loads configuration files to computer
    """
    @property
    def fullscreen(self: 'ConfigManager'):
        return self._fullscreen
    
    @fullscreen.setter
    def fullscreen(self: 'ConfigManager', value: bool):
        logger.debug('Setting fullscreen value...')
        if self._fullscreen != value:
            self._fullscreen = value
            post(Event(pygame.VIDEORESIZE))
    
    @property
    def scaled(self: 'ConfigManager'):
        return self._scaled
    
    @scaled.setter
    def scaled(self: 'ConfigManager', value: bool):
        if self._scaled != value:
            self._scaled = value
            post(Event(pygame.VIDEORESIZE))
    
    @property
    def vsync(self: 'ConfigManager'):
        return self._vsync
    
    @vsync.setter
    def vsync(self: 'ConfigManager', value: bool):
        if self._vsync != value:
            self._vsync = value
            post(Event(pygame.VIDEORESIZE))

    def __init__(self: 'ConfigManager'):
        self.game_size = copy(DEFAULT_GAME_SIZE)
        self.window_size = copy(DEFAULT_SCREEN_SIZE)
        self.player_key_mappings = [deepcopy(DEFAULT_KEYBOARD_KEYMAP)]
        self.fps = DEFAULT_FPS
        self._fullscreen = False
        self._scaled = False
        self.app_name = DEFAULT_APP_NAME
        self.author = DEFAULT_AUTHOR
        self._vsync = False
        self.master_volume = 1.0
    
    def display_mode_flags(self: 'ConfigManager'):
        flags = pygame.RESIZABLE

        if self.scaled:
            flags |= pygame.SCALED
        
        if self.fullscreen:
            flags |= pygame.SCALED | pygame.FULLSCREEN
        
        if self.vsync:
            flags |= pygame.SCALED
        
        return flags
    
    def config_path(self: 'ConfigManager') -> str:
        """Uses appdirs to save the game config to an appropriate location"""
        return path.join(user_data_dir(self.app_name, self.author), 'config.json')

    def load_config(self: 'ConfigManager'):
        """Load the game's configuration"""
        if path.exists(self.config_path()):
            print('Run logic to load configuration...')
        else:
            print('Using default configuration...')

    def save_config(self: 'ConfigManager'):
        """Save the game's configuration"""
        json_str = json.dumps(self, cls=ConfigManagerEncoder)
        logger.debug('Saving configuration: ' + json_str)
        # makedirs(self.config_path(), exist_ok=True)
        # with open(self.config_path(), 'w+', encoding="utf-8") as config_file:
        #     config_file.write(json_str)
    
    def handle_events(self: 'ConfigManager', events: list[Event]) -> None:
        relavent = [event for event in events if event.type == UPDATE_CONFIG]
        for event in relavent:
            setattr(self, event.key, event.value)

        if any(e.key in TRIGGER_SCREEN_REFRESH for e in relavent):
            post(Event(pygame.VIDEORESIZE))


class StaticConfigManager():
    instance: ConfigManager = None

    @classmethod
    def game_size(cls: 'StaticConfigManager') -> tuple[int, int]:
        return cls.instance.game_size
    
    @property
    def fullscreen(cls: 'StaticConfigManager') -> bool:
        return cls.instance.fullscreen
    
    @fullscreen.setter
    def fullscreen(cls: 'StaticConfigManager', value: bool):
        cls.instance.fullscreen = value
    
    # @property
    # def master_volume(cls: 'StaticConfigManager') -> float:
    #     return cls.instance.master_volume
    
    # @master_volume.setter
    # def master_volume(cls: 'StaticConfigManager', value: float):
    #     cls.instance.master_volume = clamp(value, 0.0, 1.0)
