from enum import Enum, auto
from pygame.joystick import Joystick


class ButtonType(Enum): # WASD  #
    UP = auto()         #   W   #
    DOWN = auto()       #   S   #
    LEFT = auto()       #   A   #
    RIGHT = auto()      #   D   #
    BUTTON_1 = auto()   #   J   #
    BUTTON_2 = auto()   #   K   #
    BUTTON_3 = auto()   #   L   #
    BUTTON_4 = auto()   #   ;   #
    BUTTON_5 = auto()   #   M   #
    BUTTON_6 = auto()   #   ,   #
    BUTTON_7 = auto()   #   \   #
    BUTTON_8 = auto()   # Enter #
    BUTTON_9 = auto()
    BUTTON_10 = auto()
    BUTTON_11 = auto()
    BUTTON_12 = auto()
    BUTTON_13 = auto()
    BUTTON_14 = auto()
    BUTTON_15 = auto()


class AxisType(Enum):
    JOYSTICK_LEFT = auto()
    JOYSTICK_RIGHT = auto()
    LEFT_TRIGGER = auto()
    RIGHT_TRIGGER = auto()
    MOUSE_POSITION = auto()
    MOUSE_WHEEL = auto()


class ControllerLayout(Enum):
    KEYBOARD = auto()


Axis = tuple[float, float]

# To Do:
#   - Add Axis
#   - Add Dead Zones
#   - Add Adjustable Dead Zones
#   - Add Toggleable Dead Zones
#   - Add Config for Xbox One Controller
#   - Add Config for PS5 Controller

ControllerMapping = dict[ButtonType, list[int]]
"""Dict with button type linked to enums representing keys"""

class Controller():
    """ Pygame Roar's interpretation of a controller

    This class uses different layouts to map keys and buttons, so that the game can be configured to use 
    whatever layout one sees fit. By leveraging this, you can have things like BUTTON_1 always be your 
    'confirm button' and not have to worry about which key or button that is.
    """
    def __init__(self, player_index: int, mapping: ControllerMapping, layout: ControllerLayout = ControllerLayout.KEYBOARD, joystick_id: int = -1):
        """Create a controller to be used by the InputManager

        Args:
            player_index (int): The index of the player. 0 refers to the first player.
            mapping (ControllerMapping): Referse to the mapping which the player uses
            layout (ControllerLayout): Which type of controller is being used
            joystick_id (int): Which joystick is associated with the player
        """
        self.player_index = player_index
        self.buttons: set[ButtonType] = set()
        self.axes: dict[AxisType, object] = {}
        self.layout: ControllerLayout = layout
        self.mapping: ControllerMapping = mapping
        self.joystick_id = joystick_id # Is this better handled by the input handler itself?
