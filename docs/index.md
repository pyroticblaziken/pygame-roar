# Welcome to Pygame Roar

Pygame Roar is a library to help manage game state. If you look at a game 
like a state transition diagram, where each screen represents a different 
state, then this library can help you build out that idea very quickly! 
This is all based on the GameStateManagementSample from way back in the 
day, provided by Microsoft for their XNA Development library.
