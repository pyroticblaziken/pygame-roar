# Usage

Pygame Roar is built around some specific ideas of how games should be implemented, or more correctly it assumes a 
certain set up for game design, based on the GameStateManagementSample from Microsoft for XNA. Below is a simple 
example of what a game might look like implemented with Pygame Roar. The big factor is extensibility so that as 
developers, you can add what you need, without having to add everything to get off the ground.

## Main Script

This script should be simple and more or less, just launch your game. The fun stuff will happen once we are inside the 
customized game itself.

```python
# ./main.py

from src.my_game import MyGame


def main():
    shouldQuit = False
    game = MyGame()
    game.init()

    while not shouldQuit:
        shouldQuit = game.run()
    
    game.quit()


if __name__ == '__main__':
    main()
```

## Game

```python
# ./src/my_game.py

from pygame_roar.game import Game
from src.components.my_audio_manager import MyAudioManager
from src.components.my_screen_manager import MyScreenManager


class MyGame(Game):
    def init_audio_manager(self: Game) -> None:
      self.audio_manager = MyAudioManager()
    
    def init_screen_manager(self: Game) -> None:
      self.screen_manager = MyScreenManager
```

## Resource Manager

Use to import resources, i.e. images, fonts, and sounds.

Can be extended like so...

All assets should include

- id
- path_parts

Fonts should include

- size
- antialias: if font should render with antialiasing or not, set to false for sprite fonts (i.e. pixel perfect fonts)
