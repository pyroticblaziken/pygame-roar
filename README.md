# Pygame Roar

Pygame extension library based on the GameStateManagementSample from Microsoft for XNA from like 2007

## Requirements

- Python 3.10
- Pygame 2.1

## Documentation

- [Pygame Roar Documentation](https://pyroticblaziken.gitlab.io/pygame-roar/)
- [MkDocs](https://gitlab.com/pages/mkdocs/-/tree/master)

## Why would you build a library like this?

So that I can think about my game based on where I am and what screens are active and the like. 
Plus this will give me a way to make more PyGame stuff more quickly.

## So what is Game State Management?

The basic concept is that a player progresses through screens to play your game, so why not make your 
game respect that idea. Everything is wrapped in a GameScreen and leverages the ScreenManager to swap 
between screens. I've also included things like input management and config management so you can save 
data and handle inputs relatively quickly without having to worry to much about it.

##